/**
 * @module awalé/player
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */
import Hole from './hole';
import {DIR_WE, DIR_EW} from './game';
/**
 * Class representing a player
 */
export default class Player {
    // static dirEW = 'EW';
    // static dirWE = 'WE';

    /**
     * Constructor
     *
     */
    constructor(name, dir) {
        this.name = name;
        this.nseeds = 0;
        this.dir = dir;
    }

    createHoles(board, hand, number)
    {
        let last, col = 'col-2', nHoles = number;

        nHoles = Math.min(nHoles, 12);
        this.holes = new Array;
        this._last = nHoles - 1;

        if(nHoles > 6) {
            col = 'col-1';
        }

        for(let i = 0; i < nHoles; i++) {
            let current = new Hole(board, hand, this, col);

            if(i > 0) {
                if(this.dir === DIR_WE) {
                    current.next = last;
                    last.previous = current;
                } else {
                    current.previous = last;
                    last.next = current;
                }
            }
            last = current;
            this.holes.push(current);
        }
    }

    abstract()
    {
        let res = new Array();
        this.holes.forEach(value => {res.push(value.nseeds)});
        res.push(this.nseeds);
        return res;
    }

    back(turn)
    {
        let setHole = function(value, index) {
            if(index < this.holes.length) {
                this.holes[index].nseeds = value;
                this.holes[index].refresh();
            }
        };
        turn.forEach(setHole.bind(this));
        this.nseeds = turn[turn.length - 1];
    }

    get first()
    {
        if(this.dir === DIR_WE) {
            return this.holes[this._last];
        } else {
            return this.holes[0];
        }
    }

    get last()
    {
        if(this.dir === DIR_WE) {
            return this.holes[0];
        } else {
            return this.holes[this._last];
        }
    }
}