/**
 * @module awalé/index
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */
import '../scss/app.scss';
import {initGame} from "./game";

document.addEventListener('DOMContentLoaded', function() {return initGame();});
