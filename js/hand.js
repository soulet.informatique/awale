/**
 * @module awalé/hand
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */
import Hole from './hole';
import Turn from './turn';
/**
 * Class representing a hole in awalé board
 */
export default class Hand
{
    /**
     * Constructor
     *
     */
    constructor(players, board)
    {
        this.players = players;
        this.player = 0;
        this.turn = 0;
        this.turns = new Array();
        this.options = Object.assign({
            tagName: 'div',
            class:   ['board'],
        });
        this.createElement(board);
    }

    /**
     * @return {undefined}
     */
    createElement(board)
    {
        this.element = document.createElement(this.options.tagName);

        this.options.class.forEach((value) => {
            this.element.classList.add(value);
        })

        board.appendChild(this.element);
        this.refresh();
    }

    backClick()
    {
        let backPlayer = function(player, index) {
          this.players[index].back(player);
        };
        let lastTurn = this.turns.pop();

        this.turn--;
        this.player = lastTurn.player;
        lastTurn.players.forEach(backPlayer.bind(this))
        this.refresh();
    }

    addButtons()
    {
        let buttons = document.getElementById('buttons');
        buttons.innerHTML = '';

        // automatic button

        // back button
        if(this.turn == 0)
            return;
        let button = document.createElement('input');
        let type = 'button';
        //Assign different attributes to the element.
        button.type = type;
        button.value = '< Coup précédent';
        button.name = type; // And the name too?
        // button.classList.add(btn btn-primary');
        button.classList.add(['btn']);
        button.classList.add(['btn-primary']);
        button.onclick = this.backClick.bind(this);

        buttons.appendChild(button);

    }

    turnDisplay()
    {
        return `<h2>Tour n°${this.turn + 1}</h2>`;
    }

    refresh()
    {
        let evaluation = [0, 0];

        if(this.turn > 0) {
            let lastTurn = new Turn(this);

            evaluation = lastTurn.eval(this.turn);
        }
        let playerDisplay = function(i, player, players, evaluation) {
            let _class = 'inactive-player';

            if(player === i)
                _class = 'active-player';
            return `<h3 class="${_class}">${players[i].name} : ${players[i].nseeds}</h3>`;
        }

        this.element.innerHTML = this.turnDisplay();
        for(let i = 0; i < this.players.length; i++) {
            this.element.innerHTML += playerDisplay(i, this.player, this.players, evaluation);
        }
        this.addButtons();
    }

    registerTurn()
    {
        this.turns.push(new Turn(this));
    }

    reversePlayer()
    {
        this.player = (this.player + 1) % this.players.length;
        this.turn++;
        this.refresh();
    }

    getActivePlayer()
    {
        return this.players[this.player];
    }
}
