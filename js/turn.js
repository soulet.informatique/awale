/**
 * @module awalé/hand
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */
import Player from './player';
/**
 * Class representing a hole in awalé board
 */
export default class Turn {
    /**
     * Constructor
     *
     */
    constructor(hand) {
        let logTurn = function(value, key, map) {
            this.players.push(value.abstract());
        }
        this.player = hand.player;
        this.players = new Array();
        hand.players.forEach(logTurn.bind(this));
    }

    countSeeds(player)
    {
        let count = 0;
        for(let i = 0; i < player.length - 1; i++) {
            count += player[i];
        }
        return count;
    }

    countHole(player)
    {
        let count = 0;
        for(let i = 0; i < player.length - 1; i++) {
            if(player[i] > 0)
            count ++;
        }
        return count;
    }

    maxSeeds(player)
    {
        let count = 0;

        for(let i = 0; i < player.length - 1; i++) {
            count = Math.max(player[i], count);
        }
        return count;
    }

    min2(player)
    {
        let count = 0;
        let max = 0;

        for(let i = 0; i < player.length - 1; i++) {
            if(player[i] < 3) {
                count++;
            } else {
                max = Math.max(max, count);
                count = 0;
            }
        }
        return count;
    }


    eval(turn)
    {
        let nseeds = function(value) {
            if(value > 23)
                value += 1000;
        }
        let res = new Array(this.players.length);

        res[0] = this.players[0][this.players[0].length - 1] * 10;
        res[1] = this.players[1][this.players[1].length - 1] * 10;

        res.reduce(nseeds);

        res[0] += this.countSeeds(this.players[0]) * Math.max(1, (20 - turn));
        res[1] += this.countSeeds(this.players[1]) * Math.max(1, (20 - turn));

        res[0] += this.countHole(this.players[0]) * 10;
        res[1] += this.countHole(this.players[1]) * 10;

        res[0] += this.maxSeeds(this.players[0]) * 10;
        res[1] += this.maxSeeds(this.players[1]) * 10;

        res[0] -= this.min2(this.players[0]) * 20;
        res[1] -= this.min2(this.players[1]) * 20;
        return res;
    }
}