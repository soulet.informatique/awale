/**
 * @module awalé/hole
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */
import Hand from './hand';
import Player from './player';

/**
 * Class representing a hole in awalé board
 * This class provides functions for features : click event on hole, game management, displaying, setter/getter on property nseeds
 */
export default class Hole
{
    /**
     * Constructor
     *
     */
    constructor(board, hand, player, col)
    {
        this.player = player;
        this.hand = hand;
        this.nseeds = 4;
        this.options = Object.assign({
            tagName: 'div',
            class:   [col],
            onClick: () => {this.clickHandler(Event)},
        });

        this.bind();
        this.createElement(board);
    }

    /*
     * event functions
     */
    bind()
    {
        this.clickHandler = this.clickHandler.bind(this);
    }

    addEventListeners()
    {
        this.element.addEventListener('click', this.clickHandler);
    }

    removeEventListeners()
    {
        this.element.removeEventListener('click', this.clickHandler);
    }

    /**
     * @param {Event} event
     * @return {undefined}
     */
    clickHandler(event)
    {
        if(!this.active())
            return;

        // init hand
        this.hand.registerTurn();
        this.target = true;
        this.hand.nseeds = this.nseeds;
        this.resolve();

        // empty hole
        this.nseeds = 0;

        //reverse player
        this.hand.reversePlayer();

        // display
        this.next.refreshAll();

        // end
        this.target = false;
    }

    /*
     * display functions
     */
    createElement(board)
    {
        this.element = document.createElement(this.options.tagName);
        this.element.classList.add(this.options.class);
        board.appendChild(this.element);
        this.addEventListeners();
        this.refresh();
    }

    refresh()
    {
        if(this.active()) {
            this.element.classList.add('active-hole');
            this.element.classList.remove('inactive-hole');
        } else {
            this.element.classList.add('inactive-hole');
            this.element.classList.remove('active-hole');
        }
        this.element.innerHTML = `<div class='seeds'><span class="seed">&nbsp&nbsp${this.nseeds}&nbsp&nbsp</span></div>`;
    }

    refreshAll()
    {
        this.refresh();
        if(!this.target) {
            this.next.refreshAll();
        }
    }

    /*
     * game functions
     */

    // test if hole can be played
    active()
    {
        if(this.hand.getActivePlayer().name !== this.player.name)
            return false;

        if(this.nseeds === 0)
            return false;
        return true;
    }

    // add seed into current hole
    resolve()
    {
        if(!this.target) {
            this.incNseeds();
            this.hand.nseeds--;
        }
        if(this.hand.nseeds === 0) {
            this.feed();
        } else {
            this.next.resolve();
        }
    }

    // check if player can feed seeds
    canReset()
    {

        if(this.hand.getActivePlayer().name === this.player.name)
            return false;
        return this.nseeds === 2 || this.nseeds === 3;
    }

    // feed seeds in current hole
    feed()
    {
        if(this.canReset()) {
            this.hand.getActivePlayer().nseeds += this.nseeds;
            this.resetSeeds();
            this.previous.feed();
        }
    }

    /*
     * browsing holes
     */
    set next(next)
    {
        this._next = next;
    }

    get next()
    {
        return this._next;
    }

    set previous(previous)
    {
        this._previous = previous;
    }

    get previous()
    {
        return this._previous;
    }

    /*
     * seeds fuctions
     */
    incNseeds()
    {
        this._nseeds++;
    }

    get nseeds()
    {
        return this._nseeds;
    }

    set nseeds(value)
    {
        this._nseeds = value;
    }

    resetSeeds()
    {
        this._nseeds = 0;
    }
}