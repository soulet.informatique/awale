/**
 * @module awalé/game
 * @author Stéphane Soulet
 * @license GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 */

import Hole from './hole';
import Hand from './hand';
import Player from './player';

export const DIR_EW = 'EW';
export const DIR_WE = 'WE';

function getBoard()
{
    return document.getElementById('board');
}

function addHr()
{
    let board = getBoard();
    let element = document.createElement("div");
    element.classList.add("col-12");
    element.innerHTML="<hr>";
    board.appendChild(element);
}

function linkHoles(hole1, hole2)
{
    hole2.previous = hole1;
    hole1.next = hole2;
}

function addPlayer(players, dir)
{
    let player = new Player(`Joueur ${players.length + 1}`, dir);

    players.push(player);
    return player;
}

export function initGame()
{
    let board = getBoard();
    let players = []
    let nHoles = 6;
    let nplayers = 2;
    let dir = DIR_WE;

    // create players
    for(let i = 0; i < nplayers; i++) {
        addPlayer(players, dir);
        dir = dir === DIR_WE ? DIR_EW : DIR_WE;
    }

    // create hand
    let hand = new Hand(players, document.getElementById('score'));

    // create holes
    for(let i = 0; i < nplayers; i++) {
        if(i !== 0) {
            addHr();
        }
        players[i].createHoles(board, hand, nHoles);
    }

    // link player boars
    for(let i = 0; i < nplayers; i++) {
        linkHoles(players[i].last, players[(i + 1) % players.length].first);
    }
}